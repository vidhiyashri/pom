package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyLeadPage extends ProjectMethods {
	
	public MyLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT,using="Create Lead") WebElement eleCreatelead;
	public CreateLeadPage clickCreatLead() {
		click(eleCreatelead);
		return new CreateLeadPage();
	}

}
